//
//  ContentView.swift
//  WoodsOfOregonApp
//
//  Created by Bria Gray on 5/2/20.
//  Copyright © 2020 Bria Gray. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            MapView()
                .edgesIgnoringSafeArea(.top)
                .frame(height: 400)
            PineTree()
                .offset(y: -130)
                .padding(.bottom, -130)
            
            VStack {
                    Text("Out of the Woods of Oregon")
                        .foregroundColor(Color("Dark Green"))
                        .bold()
                    Text("Eugene, Oregon")
                        .foregroundColor(Color.gray)
                        .multilineTextAlignment(.center)
            }
        .padding()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
