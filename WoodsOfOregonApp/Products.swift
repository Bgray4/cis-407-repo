//
//  Products.swift
//  WoodsOfOregonApp
//
//  Created by Bria Gray on 5/2/20.
//  Copyright © 2020 Bria Gray. All rights reserved.
//

import SwiftUI

struct Products: Hashable, Codable {
    var id: Int
    var name: String
    fileprivate var imageName: String
    var Category: Category
    
    enum Category: String, CaseIterable, Codable, Hashable {
        case staples =  "Staples"
        case accessories = "Accessories"
    }
    
}

extension Products {
    var image: Image {
        ImageStore.shared.image(name: imageName)
    }
}
